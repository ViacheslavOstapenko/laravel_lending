<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([], function () {
    Route::get( '/',[
        'uses' => 'MainController@index',
        'as' => 'main',
    ]);
    Route::post('/send',[
        'uses' => 'MainController@save',
    ]);
    Route::get('page/{alias}',[
        'uses' => 'PageController@show',
        'as' => 'page',
    ]);
   //Route::auth();
});
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', function () {
        $data = ['title' => 'Panel Admin'];
        return view('admin.index',$data);
    });
    //admin/pages
    Route::group(['prefix' => 'pages'],function(){
        Route::get('/',[
            'uses' => 'Admin\PagesController@index',
            'as' => 'pages',
        ]);
        //form
        Route::get('/add',[
            'uses' => 'Admin\PagesController@create',
            'as' => 'pages-add',
        ]);
        //request save in db
        Route::post('/add',[
            'uses' => 'Admin\PagesController@save',
        ]);
        Route::get('/edit/{page}',[
            'uses' => 'Admin\PagesController@show',
            'as' => 'pages-edit',
        ]);
        Route::post('/edit/{page}',[
            'uses' => 'Admin\PagesController@update',
            'as' => 'pages-update',
        ]);
        Route::delete('/delete/{page}', [
            'uses' => 'Admin\PagesController@delete',
            'as' => 'pages-delete',
        ]);
    });
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
