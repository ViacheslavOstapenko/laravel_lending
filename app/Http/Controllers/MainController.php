<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\PageRepository;
use App\Repository\ServiceRepository;
use App\Repository\PortfolioRepository;
use App\Repository\PeopleRepository;
use App\Repository\ContactRepository;
use App\Http\Requests\ContactRequest;
class MainController extends Controller
{
    //

    public function index()
    {
        $repositoryPage = new PageRepository();
        $pages = $repositoryPage->getPages();
        //new repository
        //2 service
        $repositoryService = new ServiceRepository();
        $services = $repositoryService->getService();
        //1portfolio 
        $repositoryPortfolios = new PortfolioRepository();
        $portfolios = $repositoryPortfolios->getPortfolios();
        $filters = $repositoryPortfolios->getFilters();
        //peopel
        $repositoryPeoples = new PeopleRepository();
        $peoples = $repositoryPeoples->getPeoples();

        $menu = [];

        foreach($pages as $page){
            $item = [
                'title' => $page->name,
                'alias' => $page->alias,
                
            ];
            array_push($menu, $item);
        }
        $item = ['title' => 'Services','alias' => 'service'];
        array_push($menu, $item);
        $item = ['title' => 'Portfolio', 'alias' => 'Portfolio'];
        array_push($menu, $item);
        $item = ['title' => 'Team', 'alias' => 'team'];
        array_push($menu, $item);
        $item = ['title' => 'Contact', 'alias' => 'contact'];
        array_push($menu, $item);

        //array 
        return view('site.index',[
            'pages' => $pages,
            'services' => $services,
            'portfolios' => $portfolios,
            'filters' => $filters,
            'peoples' => $peoples,
            'menu' => $menu,
        ]);
        
    }
    public function save(ContactRequest $request)
    {
            //сохранить в базу
            $data = $request->except('_token');
            $contactRepository = new ContactRepository();
            if($contactRepository->save($data)){
                return redirect()->route('main')->with('status','сообщение сохранено');
            }
            
    }
}
