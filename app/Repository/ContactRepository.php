<?php

namespace App\Repository;

use App\Contact;

class ContactRepository
{
    //
    protected $model;

    public function __construct()
    {
        $this->model = new Contact;
    }
    public function save($data)
    {
        $this->model->fill($data);
        return $this->model->save();
    }
}
