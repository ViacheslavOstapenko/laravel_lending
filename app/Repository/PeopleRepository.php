<?php

namespace App\Repository;

use App\People;

class PeopleRepository
{
    //
    protected $model;

    public function __construct()
    {
        $this->model = new People;
    }
    public function getPeoples()
    {
        return $this->model::take(3)->get();
    }
}
