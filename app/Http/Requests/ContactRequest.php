<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //правила
            'name' => 'required|max:150',
            'email' => 'required|email|max:150',
            'text' => 'required|max:500',
        ];
    }
    public function messages()
    {
        return [
            'required' => "Поле :attribute обязательно к заполнению",
            'email' => "Поле :attribute не соответствует email",
            'max' => "Поле :attribute должно содержать не болие :max символов",
        ];
    }
}
