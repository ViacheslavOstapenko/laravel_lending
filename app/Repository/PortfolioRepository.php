<?php

namespace App\Repository;

use App\Portfolio;

class PortfolioRepository
{
    //
    protected $model;

    public function __construct()
    {
        $this->model = new Portfolio;
    }
    public function getPortfolios()
    {
        return $this->model::get(['name', 'img', 'filter']);
    }
    public function getFilters()
    {
        return $this->model->distinct()->get(['filter']);
    }
}
