<?php

namespace App\Repository;

use App\Page;

class PageRepository
{
    //
    protected $model;

    public function __construct()
    {
        $this->model = new Page;
    }
    public function getPages()
    {
        return $this->model::all();
    }
    public function getPage($alias)
    {
        return $this->model::where('alias',$alias)->firstOrFail();
    }
    public function save($data)
    {
        $this->model->fill($data);
        return $this->model->save();
    }
}
