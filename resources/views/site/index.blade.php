@extends('layouts.template')
@section('header')
    @include('site.header')
@endsection

@section('content')
    @include('site.content')
@endsection

@section('service')
    @include('site.service')
@endsection

@section('portfolio')
    @include('site.portfolio')
@endsection

@section('team')
    @include('site.team')
@endsection

@section('footer')
    @include('site.footer')
@endsection





