<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Repository\PageRepository;

class PageController extends Controller
{
    //
    public function show($alias)
    {
            $repositoryPage = new PageRepository();
            $page = $repositoryPage->getPage($alias);
            $data = [
                'page' => $page,
            ];
            return view('site.page',$data);
    }
}
