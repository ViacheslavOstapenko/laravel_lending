<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\PageRepository;
use App\Http\Requests\PageRequest;
use App\Page;
use Validator;

class PagesController extends Controller
{
    //
    public function index()
    {
        $repositoryPage = new PageRepository();
        $pages = $repositoryPage->getPages();

        $data = [
            'title' => 'Pages',
            'pages' => $pages,
        ];
        return view('admin.pages',$data);
    }
    public function create()
    {
        $data = [
            'title' => 'add',
        ];
        return view('admin.pages_add',$data);
    }
    public function save(PageRequest $request)
    {
        //save to DB
        $input = $request->except('_token');
        if($request->has('images')){
            $file = $request->file('images');
            $input['img'] = $file->getClientOriginalName();
            $file->move(public_path().'/img', $input['img']);  
        }
        $pageRepository = new PageRepository();
        $pageRepository->save($input);
        return redirect()->route('pages-add')->with('status', 'страница добавлена');
    }
    public function show(Page $page, Request $request)
    {
        $old = $page->toArray();
        $data = [
            'title' => 'Редактированине страницы',
            'data' => $old,
        ];
        return view('admin.pages_edit', $data);
    }
    public function update(Request $request)
    {
        $input = $request->except('_token');
        $validator = Validator::make($input, [
            'name' => 'required|max:255',
            'alias' => 'required|max:255|unique:pages,alias,'.$input['id'],
            'text' => 'required',

        ]);
        if($validator->fails()){
            return redirect()
            ->route('pages-edit', ['page' => $input['id']])
            ->withErrors($validator);
        }
        //update DB
        return redirect()->route('pages-edit', ['page' => $input['id']])->with('status','страница обновлена');
    }
    public function delete(Page $page, Request $request)
    {
        $page->delete();
        return redirect()->route('pages')->with('status', 'страница удалена');
    }
}
