<?php

namespace App\Repository;

use App\Service;

class ServiceRepository
{
    //
    protected $model;

    public function __construct()
    {
        $this->model = new Service;
    }
    public function getService()
    {
        return $this->model::all();
    }
}